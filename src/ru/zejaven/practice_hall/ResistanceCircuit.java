package ru.zejaven.practice_hall;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;

public class ResistanceCircuit {

    private static final Function<Double, Double> straightFunction = x -> x;
    private static final Function<Double, Double> inverseFunction = x -> 1 / x;
    public static final String START_ROUND_BRACKET = "(";
    public static final String END_ROUND_BRACKET = ")";
    public static final String START_SQUARE_BRACKET = "[";
    public static final String END_SQUARE_BRACKET = "]";

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        String[] name = new String[N];
        int[] R = new int[N];
        for (int i = 0; i < N; i++) {
            name[i] = in.next();
            R[i] = in.nextInt();
        }
        in.nextLine();
        String circuit = in.nextLine();
        for (int i = 0; i < N; i++) {
            circuit = circuit.replaceAll(name[i], String.valueOf(R[i]));
        }

        List<String> str = new ArrayList<>(Arrays.asList(circuit.split(" ")));

        for (int i = 0; i < str.size(); i++) {
            i = checkRightBorder(str, i, START_ROUND_BRACKET, END_ROUND_BRACKET, straightFunction);
            i = checkRightBorder(str, i, START_SQUARE_BRACKET, END_SQUARE_BRACKET, inverseFunction);
        }
        System.out.printf("%.1f", Double.parseDouble(str.get(0)));
    }

    private static int checkRightBorder(
            List<String> str, int endBracketPosition, String startBracket, String endBracket,
            Function<Double, Double> function
    ) {
        double sum = 0;
        if (str.get(endBracketPosition).equals(endBracket)) {
            for (int startBracketPosition = endBracketPosition - 1; startBracketPosition > -1; startBracketPosition--) {
                if (str.get(startBracketPosition).equals(startBracket)) {
                    replaceList(str, startBracketPosition, endBracketPosition, function.apply(sum));
                    endBracketPosition = 0;
                    break;
                }
                sum += function.apply(Double.parseDouble(str.get(startBracketPosition)));
            }
        }
        return endBracketPosition;
    }

    private static void replaceList(List<String> str, int startBracketPosition, int endBracketPosition, double sum) {
        for (int k = 0; k < endBracketPosition - startBracketPosition; k++) {
            str.remove(startBracketPosition);
        }
        str.set(startBracketPosition, Double.toString(sum));
    }
}
