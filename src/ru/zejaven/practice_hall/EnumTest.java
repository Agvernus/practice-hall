package ru.zejaven.practice_hall;

public class EnumTest {
    public static void main(String[] args) {
        Type type = Type.A;
        type = null;
        System.out.println(type != null ? type.name() : type);
    }
}

enum Type {
    A, B, C, D
}