package ru.zejaven.practice_hall;

import java.util.List;

public class WordsFirstLetterToUpperCase {
    public static void main(String[] args) {
        System.out.println(firstLetterToUpperCase("фыва фыва фыва"));
    }

    public static String firstLetterToUpperCase(String words) {
        return List.of(words.split(" ")).stream()
                .map(str -> str.substring(0, 1).toUpperCase().concat(str.substring(1)))
                .reduce((a, b) -> a.concat(" ").concat(b)).orElse(words);
    }
}
