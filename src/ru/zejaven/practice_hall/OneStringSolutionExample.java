package ru.zejaven.practice_hall;

public class OneStringSolutionExample {
    public static void main(String[] args) {
        String str = "Калинка, малинка, калинка, моя, в саду ягода малинка, калинка моя. " +
                "Димка съел кусок калинки, и коньки отбросил, бля.";
        System.out.println(getKCount(str));
    }

    private static long getKCount(String str) {
        return str.toLowerCase().chars().filter(c -> c == 'к').count();
    }
}
