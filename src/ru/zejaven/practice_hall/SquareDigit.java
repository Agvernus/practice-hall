package ru.zejaven.practice_hall;

import java.util.Arrays;

public class SquareDigit {
    public static void main(String[] args) {
        System.out.println(squareDigit(9119));
    }

    private static int squareDigit(int n) {
        return Arrays.stream(String.valueOf(n).split(""))
                .map(s -> Integer.parseInt(s) * Integer.parseInt(s))
                .map(String::valueOf)
                .reduce((a, b) -> a + b)
                .map(Integer::parseInt)
                .orElse(0);
    }
}
