package ru.zejaven.practice_hall;

import java.util.ArrayList;
import java.util.List;

public class TestThreads {
    public static String superSomebody = "Dima";
    public static String superLoser = "Stas";
    public static String superNormal = "Mr.Nobody";

    public static void main(String[] args) throws InterruptedException {
        printPerson();
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            threads.add(new ThreadFirst());
        }
        for (Thread thread : threads) {
            thread.start();
        }

        Thread.sleep(100);
        printPerson();
    }

    private static class ThreadFirst extends Thread {

        @Override
        public void run() {
            swap();
        }

        public static void swap() {
            superNormal = superSomebody;
            superSomebody = superLoser;
            superLoser = superNormal;
            superNormal = "Mr.Nobody";
        }
    }

    public static void printPerson() {
        System.out.println("SuperMan: " + superSomebody);
        System.out.println("SuperLoser: " + superLoser);
        System.out.println("Super-normal man: " + superNormal);
    }

}
